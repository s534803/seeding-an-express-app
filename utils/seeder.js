// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file

const user = require('../data/user.json')
const info = require('../data/info.json')

// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  // User don't depend on anything else...................

  db.user = new Datastore()
  db.user.loadDatabase()

  // insert the sample data into our data store
  db.user.insert(user)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.user = db.user.find(user)
  LOG.debug(`${app.locals.user.query.length} user seeded`)

  
  // info need a customer beforehand .................................

  db.info = new Datastore()
  db.info.loadDatabase()

  // insert the sample data into our data store
  db.info.insert(info)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.info = db.info.find(info)
  LOG.debug(`${app.locals.info.query.length} info seeded`)

 

 
  LOG.info('END Seeder. Sample data read and verified.')
}
