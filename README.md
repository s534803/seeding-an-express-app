# seeding an express app..... this is our Workshop

## Github Repository URL
- [click here](https://bitbucket.org/s534803/seeding-an-express-app/src/master/) to visit repository.

## Group Members 

1. Jaswanthi Nannuru
2. Sunil Mundru
3. Rahul Mallampati

## References

1. [Express seed](https://github.com/meanie/express-seed)
2. [Seeding Database](https://scotch.io/courses/create-a-crud-app-with-node-and-mongodb/seeding-database)


