/** 
*  user model
*  Describes the characteristics of each attribute in a user resource.
*
* @author Jaswanthi Nannuru <s534803@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  sid: { type: String, required: true, unique: true },
  firstname: { type: String, required: true, default: 'Firstname' },
  familyname: { type: String, required: true, default: 'Familyname' }
})

module.exports = mongoose.model('User', UserSchema)
