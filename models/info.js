/** 
*  info model
*  Describes the characteristics of each attribute in an info resource.
*
* @author Jaswanthi Nannuru <s534803@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const InfoSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
   sid: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true},
    course: { type: String, required: true, default: 'ACS' },
 
})

module.exports = mongoose.model('Info', InfoSchema)
